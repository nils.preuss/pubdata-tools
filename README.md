# pubdata-tools

# Description

Publication data is queried and retrieved from the OpenAlex catalog of scholarly records, using python. A simple script is used, utilizing the OpenAlex API via the pyAlex library. Retrieved record data is dumped in json format to facilitate recurring or iterative analysis. A basic analysis regarding Open-Access is performed by this same script, list comprehension is used to filter records based on type and Open-Access status / features. Lastly, the analysis results are provided via simple printout.

# Installation

The script can be used as is, provided the requirements are satisfied.

## Requirements

* pyAlex library installed (use `pip install pyalex`)
* an internet connection to query record data

# Usage

The script can be run as is to provide an analysis of peer-reviewed scientific journal articles published by members of Technical University of Darmstadt (TUDa), RWTH Aachen University (RWTH) and Karlsruhe Institute of Technology (KIT) during 2016-2023. As a basic analysis regarding Open-Access, total number of publications is counted per institution per year, as well as number of Open-Access publications and number of "gold" "Open-Access" publications (i.e. primary location of publication is an Open-Access Journal).

Here are some hints for modifications, roughly in order of effort:
* Set the flag `recache = True` to retrieve the most recent data from OpenAlex, overwriting the existing dump files.
* Set the variable `years = range(2023, 2016 -1, -1)` to either an iterator or list of years you want to retrieve data for, e.g. `years = [2010, 2013, 2015]` or `years = range(2010, 2015 +1)`
* Set the variable `institution_names = ["Technical University of Darmstadt", "RWTH Aachen University", "Karlsruhe Institute of Technology"]` to a list of institutions you want to retrieve data for. You can look up eligible names at https://openalex.org/. If you get a warning that any institution name yields more than one identifier, check if the utilized one is actually the one you want. Alternatively, you can modify the script to use a pre-defined list of identifiers instead of retrieving them based on institution name.
* Modify the query / API request (filters, selected retrieval data). You can look up the usage of the pyAlex library at https://github.com/J535D165/pyalex, and use the OpenAlex documentation regarding options for filters and data selection https://docs.openalex.org/api-entities/works/filter-works
* Modify the list comprehensions to filter the retrieved data further and aggregate it. For example, set the variable `records_this_year = [item for item in records if item["publication_year"] == year and item["type"] == "book"]`. Again, refer to the OpenAlex at documentation https://docs.openalex.org/api-entities/works/filter-works or look into the data structure of the dumped json formatted records for options regarding filtering and aggregation.

## Authors and acknowledgment
The authors Nils Preuß and David Waldecker thank Anne-Christine Günther and Harald Gerlach for the fruitful discussion and feedback on the approach and implementation of this analysis.

## License
The code in this repository is licensed under the EUPL-1.2 or later. See the [LICENSE deed](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) for details.
