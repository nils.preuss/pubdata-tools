import json
from itertools import chain, product
from pathlib import Path
from warnings import warn
from pyalex import config, Works, Institutions


def get_institution_id(display_name):
    institution_query = Institutions() \
        .search_filter(display_name=display_name) \
        .get()

    if len(institution_query) > 1:
        warn(f"Looking up {display_name} yields more than one ID, using {institution_query[0]["id"]}", UserWarning)

    return institution_query[0]["id"]


config.email = "nils.preuss@tu-darmstadt.de"

institution_names = [
    "Technical University of Darmstadt",
    "RWTH Aachen University",
    "Karlsruhe Institute of Technology"
    ]

years = range(2023, 2016 - 1, -1)

recache = False

for institution_name, year in product(institution_names, years):
    filename = Path(f"./data/OpenAlex records - {year} - {institution_name}.json")
    if recache is False and filename.is_file():
        continue

    institution_id = get_institution_id(institution_name)

    query = Works() \
        .filter(publication_year=f"{year}") \
        .filter(authorships={"institutions": {"lineage": institution_id}}) \
        .filter(is_paratext=False) \
        .filter(type="!erratum") \
        .filter(primary_location={"version": "!submittedVersion"}) \
        .select(["doi", "type", "publication_year", "primary_location",
                 "open_access", "apc_list"]) \
        .paginate(per_page=200, n_max=None)

    print(f"dumping '{filename}'")

    with filename.open("w") as dumpfile:
        json_array = []
        for record in chain(*query):
            json_array.append(record)

        json.dump(json_array, dumpfile, indent=4)

for institution_name, year in product(institution_names, years):
    filename = Path(f"./data/OpenAlex records - {year} - {institution_name}.json")
    with filename.open("r") as dumpfile:
        records = json.load(dumpfile)

        records_this_year = [item for item in records if item["publication_year"] == year
                             and item["type"] == "article"
                             and (item["primary_location"] is not None
                                  and item["primary_location"]["source"] is not None
                                  and item["primary_location"]["source"]["type"] == "journal")]
        records_this_year_oa = [item for item in records_this_year if item["open_access"]["is_oa"] is True]
        records_this_year_gold = [item for item in records_this_year_oa if item["open_access"]["oa_status"] == "gold"
                                  or (item["primary_location"] is not None
                                      and item["primary_location"]["source"] is not None
                                      and item["primary_location"]["source"]["is_oa"] is True)]

        print(f"{institution_name} "
              f"{year}: {len(records_this_year)}, "
              f"OA: {len(records_this_year_oa)} "
              f"({len(records_this_year_oa) / len(records_this_year) * 100:.0f} %), "
              f"gold OA: {len(records_this_year_gold)} "
              f"({len(records_this_year_gold) / len(records_this_year_oa) * 100:.0f} %)")
